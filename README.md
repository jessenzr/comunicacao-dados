# INTRODU��O<a id="orgheadline1"></a>

Desenvolver um projeto com ardu�no envolvendo comunica��o via Bluetooth com um smartphone. Este projeto visa como resultado o aprendizado sobre a utiliza��o de Shields no ardu�no, assim como efetuar a comunica��o via Bluetooth entre os dispositivos via programa��o.

# COMPONENTES<a id="orgheadline2"></a>

-   1 Arduino UNO
-   1 Resistor de 150 ohms
-   1 M�dulo de Bluetooth para Arduino
-   1 LED
-   1 Smartphone
-   Jumpers

# Sobre o bluetooth
Um dispositivo de bluetooth usa ondas de r�dio ao inv�s de fios ou cabos para se conectar a um telefone ou computador. Dispositivos de bluetooth cont�m um pequeno chip equipado com um firmware com o objetivo de deixar a conex�o o mais simples poss�vel. Ao conectar dispositivos de bluetooth, � necess�rio pare�-los.

# Conex�es
O m�dulo de bluetooth tem 4 pinos para conectar ao ardu�no:
- RXD: recebe dados do ardu�no
- TXD: envia dados ao ardu�no
- VCC: fonte de energia
- GND: terra

```
Arduino-------------HC-06
RX-------------------TXD
TX-------------------RXD
+5V-----------------VCC
GND----------------GND
```

# PROJETO<a id="orgheadline3"></a>
![img](caso-uso.png)

[Montagem da parte eletr�nica](http://www.instructables.com/id/How-control-arduino-board-using-an-android-phone-a/)

![img](esquema.png)

C�digo fonte desenvolvido:

    char command;
    String string;
    #define led 13
    
    bool piscando;
    
    void setup() {
      Serial.begin(9600);
      pinMode(led, OUTPUT);
    }
    
    
    void loop() {
      if (Serial.available() > 0)
        {string = "";}
         while(Serial.available() > 0)
          {
           command = ((byte)Serial.read());
           if(command == ':')
           {
        break;
           }
           else
            {
              string += command;
           }
            delay(1);
         }
          if(string == "LO")
          {
            piscando = false;
            LEDOn();
          }
          if(string =="LF")
          {
            piscando = false;
            LEDOff();
          }
    
    
          if(string == "LP") {
            piscando = true;
          }
    
    
      if (piscando) {
        delay(500);
        LEDOn();
        delay(500);
        LEDOff();
      }
    }
    
    
    void LEDOn()
    {
      digitalWrite(led, HIGH);
    }
    
    
    void LEDOff()
    {
      digitalWrite(led, LOW);
    }

# COMO FUNCIONA<a id="orgheadline4"></a>

Foi utilizado um app no smartphone chamado UnWired Lite, onde o mesmo utiliza a comunica��o serial do bluetooth para enviar linhas de comando. O ardu�no foi programado para que, ao receber determinado comando, realizasse a��es com o LED:

-   Ligar LED
-   Desligar LED
-   Fazer o LED piscar

# RESULTADO<a id="orgheadline5"></a>

Encontramos algumas dificuldades na execu��o do projeto devido a sincroniza��o entre o ardu�no e o smartphone, onde tivemos que repetir o envio do comando algumas vezes para o ardu�no process�-lo. Outro problema encontrado foi um erro que receb�amos ao realizar o upload do c�digo fonte para o ardu�no. Ap�s muita pesquisa, foi descoberto que � necess�rio desenergizar o m�dulo do bluetooth antes de realizar o upload. Mas, apesar dessas dificuldades, conseguimos obter os resultados esperados.
Com este projeto conseguimos aprender a realizar a comunica��o entre Android e ardu�no utilizando bluetooth. Mesmo sendo um projeto um tanto quanto simples, nos agregou conhecimento e despertou interesse e criatividade para v�rias implementa��es utilizando ardu�no e Android.

# IMAGENS DO PROJETO<a id="orgheadline6"></a>

![img](montado-2.jpg)
![img](montado-3.jpg)

# Bibliografia
http://www.instructables.com/id/LED-Control-using-Arduino-Bluetooth-and-Android-Pa/
